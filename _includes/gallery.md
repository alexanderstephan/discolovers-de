---

layout: page
title: Gallery
permalink: /galerry/
---
<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/155232150@N04/38587014664/in/dateposted-public/" title="Peaceful Valley"><img src="https://farm5.staticflickr.com/4739/38587014664_689ed5848b_c.jpg" width="800" height="600" alt="Peaceful Valley"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/155232150@N04/27518439089/in/dateposted-public/" title="LivelyPlanet"><img src="https://farm5.staticflickr.com/4595/27518439089_56418f9ceb_c.jpg" width="800" height="600" alt="LivelyPlanet"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/155232150@N04/39265869812/in/dateposted-public/" title="Landscape"><img src="https://farm5.staticflickr.com/4682/39265869812_13612b1153_c.jpg" width="800" height="600" alt="Landscape"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
