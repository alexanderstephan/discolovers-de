---
layout: page
title: About
permalink: /about/
---

Hi, Alex here.

I am a 20 year old student from Bavaria. I run a small start up called [Alpine Keyboards][alpine-keyboards].
Besides that I sometimes stream some DJ Sets on [chew.tv][chew-tv]. Also, I love japanese green tea.


In my blog I mainly write about programming, mechanical keyboards and technology.
You will see build logs and even actual instructions covering various topics.


[chew-tv]: https:/chew.tv
[alpine-keyboards]:https://alpine-keyboards.com
